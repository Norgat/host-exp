(ns hello
  (:require [clojure.browser.event :as event]
            [clojure.browser.dom   :as dom]
            [applet :as app]
            [core :as ql])
  (:use-macros [tools :only [with-applet defapplet]]))



(defn draw-fn []
  (ql/background 255)
  (ql/stroke 0)
  (ql/curve 5 26 5 26 73 24 73 61)
  (ql/curve 5 26 73 24 73 61 15 65)
  (ql/curve 73 24 73 61 15 65 15 65))


(defapplet example
  :draw draw-fn
  :host "host")
