(defproject host-exp "0.1.0-SNAPSHOT"
  :description "Experiments with Processing.js hosting on html page."
  :url "http:vk.com/norgat"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :plugins [[lein-cljsbuild "1.0.2"]]

  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/clojurescript "0.0-2173"]]

  :cljsbuild
  {:builds [{
             :source-paths ["src"]
             :compiler
             {:output-to "resources/js/main.js"
              :optimizations :whitespace
              :libs ["resources/js/processing-1.4.1.js"]
              :pretty-print true}}]}
  )
